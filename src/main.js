var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var maxSize = 0.1
var steps = 32
var dimension = 5
var frame = 0

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var k = 0; k < dimension; k++) {
    for (var j = 0; j < dimension; j++) {

      push()
      translate((j - Math.floor(dimension * 0.5)) * boardSize * maxSize * 1.5, (k - Math.floor(dimension * 0.5)) * boardSize * maxSize * sqrt(3) + (j % 2) * boardSize * maxSize * sqrt(3) * 0.5)
      for (var i = steps; i > 0; i--) {
        if (j % 2 === 1 && k < dimension - 1 || j === 0 && k !== 0 && k !== dimension - 1 || j === Math.floor(dimension * 0.5) || j === dimension - 1 && k !== 0 && k !== dimension - 1) {
          push()
          translate(windowWidth * 0.5, windowHeight * 0.5)
          noStroke()
          fill(255 - 255 * ((i + j + k) * (frame * 0.25) % (steps * steps * 0.125) / steps))
          polygon(0, 0, boardSize * maxSize * (i / steps), 6)
          pop()
        }
      }
      pop()
    }
  }

  frame += deltaTime * 0.01
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function polygon(x, y, radius, npoints) {
  let angle = TWO_PI / npoints
  beginShape()
  for (let a = 0; a < TWO_PI; a += angle) {
    let sx = x + cos(a) * radius
    let sy = y + sin(a) * radius
    vertex(sx, sy)
  }
  endShape(CLOSE)
}
